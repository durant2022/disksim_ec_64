/*
 * @Author: your name
 * @Date: 2021-06-30 03:32:40
 * @LastEditTime: 2021-06-30 19:01:03
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /LoadBalancerSim/src/qsort.c
 */
#include <stdio.h>
#include <stdlib.h>
#include <stripe_utils.h>
//此方法中，存储记录的数组中，下标为 0 的位置时空着的，不放任何记录，记录从下标为 1 处开始依次存放
int Partition(int *L,int low,int high){
    int pivotkey= L[low];
    //直到两指针相遇，程序结束
    while (low<high) {
        //high指针左移，直至遇到比pivotkey值小的记录，指针停止移动
        while (low<high && L[high]>=pivotkey) {
            high--;
        }
        //直接将high指向的小于支点的记录移动到low指针的位置。
        L[low] = L[high];
        //low 指针右移，直至遇到比pivotkey值大的记录，指针停止移动
        while (low<high && L[low]<=pivotkey) {
            low++;
        }
        //直接将low指向的大于支点的记录移动到high指针的位置
        L[high]=L[low];
    }
    //将支点添加到准确的位置
    L[low]=pivotkey;
    return low;
}
void QSort(int *L,int low,int high){
    if (low<high) {
        //找到支点的位置
        int pivotloc=Partition(L, low, high);
        //对支点左侧的子表进行排序
        QSort(L, low, pivotloc-1);
        //对支点右侧的子表进行排序
        QSort(L, pivotloc+1, high);
    }
}
void QuickSort(int *vec, int len){
    QSort(vec, 0,len-1);
}