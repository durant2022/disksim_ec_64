/*
 * @Author: Durant Thorvalds
 * @Date: 2021-06-25 00:35:47
 * @LastEditTime: 2021-07-13 22:37:29
 * @LastEditors: Please set LastEditors
 * @Description: This is a Test file for SGA
 * @FilePath: /LoadBalancerSim/src/main.c
 */
#include "stripe_utils.h"
#include "ECconfig.h"

FILE* log_fp;
BALANCE_SCHEME BALANCE_SCHEME0;
SCHEDULE_SCHEME SCHEDULE_SCHEME0;
int failed_diskno = 1;
SGA *sga;
#define node_num 8 
#define ecK  2
#define ecM  2
#define ecN  (ecK+ecM)   
#define stripe_num  10000
 
#define failed_diskno   1 //,\
                        2,\
                        3 
#define PARTITION 25 
#pragma GCC optimize(2)
#define DEBUG_MODE false
#define LOG_MODE true
#define GEN_OR_READ_DIST false
#define DIST_PATH "/home/disksim/DISKSIM64_C/dist/dist.data" 
#define LOG_PATH  "/home/disksim/DISKSIM64_C/logs/log.txt" //"stdout" 
#define RAND_SEED 2021
int main()
{

    if (node_num < ecN || ecK > ecN) {
        fprintf(stderr, "input erasure code parameters error!\n");
        exit(-1);
    }
    log_fp = stdout;
    if(LOG_PATH != "stdout" )
    {
        FILE* fp = fopen(LOG_PATH,"a");
        if(fp == NULL)
        {
            fprintf(stderr, "open log_fp error %d!\n", errno);
            exit(-1);
        }
        log_fp = fp;
    }

    sga = (SGA* )calloc(1,sizeof(SGA));
    int **new_dist;
    fprintf(log_fp,"Parameters: ecK:%d, ecM:%d, NodeNum:%d, StripeNum:%d, RandSeed:%d\n",
                           ecK ,ecM, node_num, stripe_num,RAND_SEED);
    
    set_params(node_num,ecN,ecK,stripe_num,RAND_SEED,INT32_MAX);
    

    if(GEN_OR_READ_DIST)
    {
        fprintf(log_fp,"Begin generating random distribution...\n");
        generate_random_distributions(false);
        fprintf(log_fp,"Finish generating random distribution, Write to disk..\n");
        write_distribution_to_disk(DIST_PATH);
        fprintf(log_fp,"Write done!\n");
        
    }else
    {
        read_distribution_from_disk( DIST_PATH);
        fprintf(log_fp,"Read done!\n");
    } 
    if(!checkStripeRepair())
    {
        fprintf(stderr,"Oops, Somthing wrong in checkStripeRepair(), please check\n");
        exit(-1);
    }

    // print_dist(sga->dist);

    all_schemes_generator();//this should always at the frontier

    fprintf(log_fp,"\n>>BASE TEST STARTS \n");
    BALANCE_SCHEME0 = BASE;
    IO_load_balancer();

    fprintf(log_fp,"\n>>LOAD BALANCE TEST STARTS \n");
    BALANCE_SCHEME0 = LOAD_BALANCE;
    // print_dist(sga->dist);
    new_dist = IO_load_balancer();
    graph_generator(new_dist);
    SCHEDULE_SCHEME0 = COLORING_SEQ;
    getMinimalTime();
    SCHEDULE_SCHEME0 = COLORING_CLIQUE;
    getMinimalTime();
    // print_dist(new_dist);
    free_arr2D(sga->load_blncd_dist,sga->_stripe_num);
    free_arr2D(sga->diskload_dict, sga->_node_num);
    free_arr2D(sga->cyclicGraph, sga->failStripeNum);
    free_arr2D(sga->matchStripeSet, sga->minTimeSlice);
    
    
    fprintf(log_fp,"\n>>RANDOM TEST STARTS \n");
    BALANCE_SCHEME0 = RANDOM;
    // print_dist(sga->dist);
    new_dist = IO_load_balancer();
    // print_dist(new_dist);
    SCHEDULE_SCHEME0 = COLORING_SEQ;
    graph_generator(new_dist);
    getMinimalTime();
    free_arr2D(sga->rand_dist,sga->_stripe_num); //cause Double Free Problem
    free_arr2D(sga->cyclicGraph, sga->failStripeNum);
    free_arr2D(sga->matchStripeSet, sga->minTimeSlice);

    // print_dist(sga->dist);
    
    fprintf(log_fp,"\n>>GREEDY TEST STARTS \n");
    BALANCE_SCHEME0 = GREEDY;
    // print_dist(sga->dist);
    new_dist = IO_load_balancer();
    // print_dist(new_dist);
    SCHEDULE_SCHEME0 = COLORING_SEQ;
    graph_generator(new_dist);
    getMinimalTime();
    free_arr2D(sga->greedy_dist,sga->_stripe_num);
    free_arr2D(sga->cyclicGraph, sga->failStripeNum);
    free_arr2D(sga->matchStripeSet, sga->minTimeSlice);
    
    
    // print_dist(sga->dist);
    
    destroy_sga();
    free(sga);
    sga = NULL;
    fflush(log_fp);
    fclose(log_fp);
    return 0;

}   