/*
 * @Author: your name
 * @Date: 2021-06-28 23:25:26
 * @LastEditTime: 2021-07-20 02:49:06
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /LoadBalancerSim/src/temp.c
 */
/*
 * @Author: Durant 
 * @Date: 2021-06-25 00:03:17
 * @LastEditTime: 2021-06-28 23:18:52
 * @LastEditors: Please set LastEditors
 * @Description: Stripe utils
 * @FilePath: /LoadBalancerSim/src/stripeSGA.c
 */
#include "stripe_utils.h"
#include "disksim_global.h"
/**
 * @description: initialize a 2-D array pointer
 * @param {int} row, the row number 
 * @param {int} col, the column number
 * @return {int**} a new 2-D array pointer
 */
char* DIST_PATH;
#define MAXDEVICES	400
//utils
void free_ex(void* ptr)
{
    if (ptr == NULL) return;
    free(ptr);
    ptr= NULL;
}
int** init_arr2D(int row, int col)
{
    int **arr = (int**)calloc(row , sizeof(int*));
    for(int i = 0;i < row;i++)
        arr[i] = (int*) calloc(col, sizeof(int));
    return arr;
}
void free_arr2D(int** arr, int len)
{
    if(arr == NULL) return;
    for(int i = 0;i < len;i++)
        free_ex(arr[i]);
    free_ex(arr);
}
int*** init_arr3D(int depth, int row, int col)
{
    
    int ***arr = (int***)calloc(depth , sizeof(int**));
    for(int d = 0;d < depth; d++)
    {
        arr[d] = (int**)calloc(row , sizeof(int*));
        for(int i = 0;i < row;i++)
            arr[d][i] = (int*) calloc(col, sizeof(int));
    }
    return arr;
}
void free_arr3D(int*** arr, int row ,int col)
{
    for(int i = 0;i < row;i++)
    {
        for(int j = 0;j < col;j++)
            free_ex(arr[i][j]);
        free_ex(arr[i]);
    }
    free_ex(arr);
}

void safe_free_sga()
{
    if(BALANCE_SCHEME0 == LOAD_BALANCE)
    {
        free_arr2D(sga->diskload_dict, sga->_node_num);
    }
    else if(BALANCE_SCHEME0 == GREEDY)
    {
        vector_destroy(sga->allSchemes);
    }
    else if(BALANCE_SCHEME0 == RANDOM)
    {
        vector_destroy(sga->allSchemes);
    }
    free_arr2D(sga->cyclicGraph, sga->failStripeNum);
    free_ex(sga->stripesColor);
    free_ex(sga->final_read_scheme);
    free_ex(sga->final_write_scheme);
}


void print_vec(char* prefix, int *vec, int len)
{
    fprintf(log_fp, prefix);
    for(int i = 0;i < len; i++)
    {
        fprintf(log_fp, " %d,", vec[i]);
    }
    fprintf(log_fp, "\n");
}

/**
 * @description: set_params
 * @param {*}
 * @return {*}
 */
void set_params( int _node_num, int _ecN, 
                                int _ecK, int _stripe_num, 
                                int _rand_seed, 
                                int _maxStripeRepairPerSlice)
{  
    sga->_node_num = _node_num;
    sga-> _ecN = _ecN;
    sga->_ecK = _ecK;
    sga->_stripe_num = _stripe_num;
    sga->_rand_seed = _rand_seed;
    sga->_maxStripeRepairPerSlice = _maxStripeRepairPerSlice>_stripe_num
                                ?_stripe_num:_maxStripeRepairPerSlice;
    sga->dist = init_arr2D(_stripe_num, _node_num );
    sga->failNodeNum = 0;
    sga->_failNodeSet = (int *)calloc(MAXDEVICES , sizeof(int));
    sga->_backupSet = (int *)calloc(MAXDEVICES , sizeof(int));
    
}

void transfer(char * src1, char *src2)
{
    if(strcasecmp (src1 , "LOAD_BALANCE") == 0)
        BALANCE_SCHEME0 = LOAD_BALANCE;
    else if(strcasecmp (src1 , "GREEDY") == 0)
        BALANCE_SCHEME0 = GREEDY;
    else if(strcasecmp (src1 , "RANDOM") == 0)
        BALANCE_SCHEME0 = RANDOM;
    else 
    {
        fprintf(stderr," Load balance scheme is invalid!\n");
        exit(-1);
    }
    if(strcasecmp (src2 , "NONE") == 0)
        SCHEDULE_SCHEME0 = NONE;
    else if(strcasecmp (src2 , "COLORING_SEQ") == 0)
        SCHEDULE_SCHEME0 = COLORING_SEQ;
    else if(strcasecmp (src2 , "COLORING_CLIQUE") == 0)
        SCHEDULE_SCHEME0 = COLORING_CLIQUE;
    else if(strcasecmp (src2 , "COLORING_BFS") == 0)
        SCHEDULE_SCHEME0 = COLORING_BFS;
    else 
    {
        fprintf(stderr," IO Scheduling is invalid\n");
        exit(-1);
    }
}
bool checkStripeRepair()
{
    if(BALANCE_SCHEME0 == LOAD_BALANCE)
        fprintf(log_fp," Load_balance scheme is LOAD_BALANCE\n");
    else if(BALANCE_SCHEME0 == GREEDY)
        fprintf(log_fp," Load_balance scheme is GREEDY\n");
    else if(BALANCE_SCHEME0 == RANDOM)
        fprintf(log_fp," Load_balance scheme is RANDOM\n");
    else 
        fprintf(log_fp," Load_balance scheme is NONE\n");

    if(SCHEDULE_SCHEME0 == COLORING_SEQ)
        fprintf(log_fp," IO Scheduling scheme is COLORING_SEQ\n");    
    else if(SCHEDULE_SCHEME0 == COLORING_BFS)
        fprintf(log_fp," IO Scheduling is COLORING_BFS\n");   
    else if(SCHEDULE_SCHEME0 == COLORING_CLIQUE)
        fprintf(log_fp," IO Scheduling is COLORING_CLIQUE\n");   
    else 
        fprintf(log_fp," IO Scheduling is NONE\n");
  
    //check routine
    int  fail_arr[] = {failed_diskno};
    sga->failNodeNum = sizeof(fail_arr)/sizeof(fail_arr[0]);
    sga->final_read_scheme = (int *) calloc(sga->_node_num, sizeof(int));
    sga->final_write_scheme = (int *) calloc(sga->_node_num, sizeof(int));
    if(!sga->failNodeNum)
    {
        fprintf(stderr,"No failed node found!\n");
        return false;
    }
    if(sga->failNodeNum >  (sga->_ecN - sga->_ecK))
    {
        fprintf(stderr,"failed num shoule be less than N-K!\n");
        return false;   
    }
    sga->_maxRepairNumPerSlice = (sga->_node_num - sga->failNodeNum)/(sga->_ecN - sga->_ecK);
    fprintf(log_fp,"The maximum stripes could be repaired in parallel is %d\n", sga->_maxRepairNumPerSlice);
    
    
    
    for(int f = 0;f < sga->failNodeNum;f++)
    {
        sga->_failNodeSet[fail_arr[f]] = 1;
        fprintf(log_fp,"node%2d,", fail_arr[f]);
    }
    fprintf(log_fp,"failed \n");

    sga->failStripeNum = 0;
    for(int i = 0;i < sga->_stripe_num; i++)
    {
        for(int f = 0;f < sga->failNodeNum;f++)
        if(sga->dist[i][fail_arr[f]]) 
        {
            sga->failStripeNum ++;
            break;
        }
    }
    sga->minTimeSlice = sga->failStripeNum;
    
    sga->stripesToRepair = (int *) calloc(sga->failStripeNum, sizeof(int));
    
    int k = 0;
    for(int i = 0;i < sga->_stripe_num; i++)
    {
        for(int f = 0;f < sga->failNodeNum;f++)
        if(sga->dist[i][fail_arr[f]]) 
        {
            sga->stripesToRepair[k++] = i;
            break;
        }
    }
    sga->cyclicGraph = init_arr2D(sga->failStripeNum, sga->failStripeNum);


    if(!sga->failStripeNum)
    {
        fprintf(stderr,"No stripe needs repair!\n");
        return false;
    }
    fprintf(log_fp,"totally %d stripes need repair\n", sga->failStripeNum);
    
    return true;
    
   
}

void destroy_sga()
{
    if(BALANCE_SCHEME0 == LOAD_BALANCE)
    {
        free_arr2D(sga->load_blncd_dist,sga->_stripe_num);
    }
    else if(BALANCE_SCHEME0 == GREEDY)
    {
        free_arr2D(sga->greedy_dist,sga->_stripe_num);
    }
    else if(BALANCE_SCHEME0 == RANDOM)
    {
        free_arr2D(sga->rand_dist,sga->_stripe_num);
    }
    
    // print_dist(sga->dist);
    free_arr2D(sga->dist, sga->_stripe_num);
    free_ex(sga->_failNodeSet);
    free_ex(sga->stripesToRepair);
    free_arr2D(sga->matchStripeSet, sga->minTimeSlice);
    free_ex(sga->sumBlocks);
}

/**
 * @description: print the data distribution
 * @param {*}
 * @return {*}
 */
void print_dist(int** dist )
{
    fprintf(log_fp, "nodeID:  ");
    
    for(int i = 0;i < sga->_node_num; i++)
    {
        fprintf(log_fp, "%*d ", 4, i+1);
    }
    fprintf(log_fp,"\n");
    for(int i = 0;i < sga->_stripe_num; i++)
    {
        fprintf(log_fp, "stripe%2d:", i);
        for(int j = 0;j < sga->_node_num; j++)
        {
            fprintf(log_fp, "%*d ",4, dist[i][j]);
        }
        fprintf(log_fp,"\n");
    }
}

/**
 * @description:  generate random and even distribution
 *                the data chunks are labeled with integer ranging from 1 to k
 *                the parity chunks are from k+1 to k+m
 * @param vector_t* &dist: distreibution to be generated
 * @param {bool} print: whether or not to print the generated stripes 
 * @return {*}
 * @author: Durant Thorvals
 * @complexity: Time:O(_stripe_num*(_ecN+_ecK)),Space:O(_node_num)
 */
int ** generate_random_distributions( bool print)
{

    vector_t* nodeV = create_vector(int);
    vector_init_n(nodeV, sga->_node_num);
    sga->sumBlocks = (int *)calloc(sga->_node_num,sizeof(int));
    
    for(int i = 0;i < sga->_node_num; i++)
    {
        vec_at(nodeV,i) = i;
    }
    srand(sga->_rand_seed);
    for(int j = 0;j < sga->_stripe_num;j++)
    {
        algo_random_shuffle(vector_begin(nodeV), vector_end(nodeV));
        // rand_shuffle(nodeV, sga->_node_num);
        // for(int i = 0;i < sga->_node_num;i++)
        //     printf("%d,",nodeV[i]);
        // printf("\n");
        //deploy raw blocksx
        for(int k = 0;k < sga->_ecK; k++)
        {
            sga->dist[j][ vec_at(nodeV,k)] = k+1;
            sga->sumBlocks[vec_at(nodeV,k)]++;
        }
        //deploy parity blocks
        for(int l = sga->_ecK;l < sga->_ecN; l++)
        {
            sga->dist[j][vec_at(nodeV,l)] = l+1;
            sga->sumBlocks[vec_at(nodeV,l)]++;
        }
    }
    
    if(!print) {
        vector_destroy(nodeV);
        return;
    }
    
    print_dist(sga->dist);
    fprintf(log_fp, "\nblockSum: ");
    
    for(int i = 0;i < sga->_node_num; i++)
    {
        fprintf(log_fp, "%*d ", 4, sga->sumBlocks[i]);
    }
    fprintf(log_fp,"\n");
    vector_destroy(nodeV);
}

/**
 * @description: Write generated stripe distributin to destinated path
 * @param {vector_t*} &dist
 * @param {string} path
 * @return {*}
 * @author: Durant Thorvals
 * @complexity: Time:O(1),Space:O(1)
 */
void write_distribution_to_disk( char* path)
{
    if(sga->dist == NULL) return;
    // fprintf(log_fp, "Error creating %s!:%d\n", path, errno);
    FILE *fp = fopen(path, "w+");
    if(fp == NULL)
    {
        extern int errno;
        fprintf(log_fp, "Error creating %s!:%d\n", path, errno);
        exit(-1);
    }
    
    for(int i = 0;i < sga->_stripe_num; i++)
    {    for(int j = 0;j < sga->_node_num; j++)
        {
            char *num;
            sprintf(num,"%d", sga->dist[i][j]);
            fputs (num, fp);
            if(j < sga->_node_num - 1)
                fputs("\t", fp);
            else    
                fputs("\n", fp);
        }
    }    
    fflush(fp);
    fclose(fp);
}

/**
 * @description: read stripe distribution from disk
 * @param {string} path
 * @return {*}
 * @author: Durant Thorvals
 * @complexity: Time:O(1),Space:O(1)
 */
void  read_distribution_from_disk( char *path)
{
    
    FILE* fp = fopen(path, "r");
    if(fp == NULL)
    {
        extern int errno;
        fprintf(log_fp, "Error reading %s!:%d\n", path, errno);
        exit(-1);
    }
    char buff[255];
    int line = 0, col = 0, num = 0, ecCnt = 0;
    

    while(!feof(fp))
    {
        if(fgets(buff,255,fp)!=NULL) {
            char *rest = NULL;
            char* t = __strtok_r(buff,"\t", &rest);
            col = 0;
            ecCnt = 0;
            for(int i = 0; t[i]!='\n'; i++)
            if( t[i] >= '0' && t[i] <= '9')
            {
                if(t[i+1] >= '0' && t[i+1] <= '9')
                {//assume the disk less than 100, so just consider decimal
                    num = (int)(t[i]-'0')*10 + (int)t[i+1] - '0';
                    if(t[i]!='0') ecCnt ++;
                    i++;
                }
                else 
                {
                    if(t[i]!='0') ecCnt ++;
                    num = (int)(t[i]-'0');
                }
                if(col >= sga->_node_num||line >= sga->_stripe_num) 
                {
                    fprintf(stderr,"the dist file col1 doesnot match ec parameter!\n");
                    fflush(fp);
                    fclose(fp);
                    exit(-1);
                }
                sga->dist[line][col] = num;
                col++;
                
            }

            if(ecCnt != sga->_ecN || col != sga->_node_num) 
            {
                fprintf(stderr,"the dist file col2 doesnot match ec parameter!\n");
                fflush(fp);
                fclose(fp);
                exit(-1);
            }
            line++;
        }


    }
    if(line != sga->_stripe_num) 
    {
        fprintf(stderr,"the dist file line doesnot match ec parameter!\n");
        fflush(fp);
        fclose(fp);
        exit(-1);
    }
    fflush(fp);
    fclose(fp);


}




/**
 * @description: generate all possible scheme unless customized schemes are provided
 * @param {vector_t*} &dist
 * @return {*}
 */
void all_schemes_generator()
{
    sga->allSchemes = create_vector(vector_t<vector_t<int>>);
    vector_init(sga->allSchemes);
    int fail_arr[] = {failed_diskno};
    if(vector_empty (sga->allSchemes))
    {
        vector_resize(sga->allSchemes, sga->failStripeNum);
        int* rowVec = (int*) calloc(sga->_node_num, sizeof(int));
        for(int i = 0; i < sga->failStripeNum; ++i)
        {
            //if there are so many branches ,we must prune some   
            
            for(int k = 0; k < sga->_node_num;k++)
                rowVec[k] = sga->dist[sga->stripesToRepair[i]][k]; 
            for(int f = 0;f < sga->failNodeNum;f++)
                rowVec[fail_arr[f]]= 0;//The failed node is marked as 0

            //we search for the subset which contain k 1's.
            // vector_init_copy(ret,vector_at(sga->allSchemes,i));
            // print_vec("row_vec:", rowVec, sga->_node_num);
            generate_default_repair_schemes( 0, sga->_ecN-1, rowVec,i);
            

        }

        free_ex(rowVec);
            
    }
}

/**
 * @description: By LOAD_BALANCED, we have RS that every k blocks could 
 *                     be used to repair the one failed node
 * @param {vector_t*} &dist
 * @return {*}
 * @author: Durant Thorvals
 * @complexity: Time:O(C_{k+m-1}^k),Space:O(C_{k+m-1}^k)
 */
void generate_default_repair_schemes( int start, int cntK, int *rowVec, int stripe)
{
    //The combination number is C_{k+m-1}^k
    //we search for the subset which contain k 1's.

    if(cntK == sga->_ecK)
    {
        // print_vec(" ",rowVec);
        vector_t *tmp = create_vector(int);
        vector_init_copy_array(tmp,rowVec,sga->_node_num);
        vector_push_back(vector_at(sga->allSchemes,stripe), tmp);
        vector_destroy(tmp);
        return;
    }
    //randomly pick k blocks from k+m-1 nonzero blocks
    
    for(int i = start+1 ;i < sga->_node_num; i++)
    if(rowVec[i])
    {
        int tmp = rowVec[i];
        rowVec[i] = 0;
        generate_default_repair_schemes(i, cntK-1, rowVec, stripe);
        rowVec[i] = tmp;
    }
    
}

bool all_num(int * arr, int len, int num)
{
    for(int i = 0;i < len;i++)
    if(arr[i]!=num) return false;

    return true;
}
/**
 * @description: the IO-load balancer, generate the best scheme as far as load balance is concerned
 * @param {vector_t*} &dist
 * @param {vector_t*} &repairSchemes: customized repair scheme
 * @return {vector_t*} the load balanced distribution  
 * @author: Durant Thorvals
 * @complexity: Time:O(N*(m-1)*|f|)
 */
int** IO_load_balancer()
{
    // If there is only one recovering scheme then return
    for(int i = 0;i < sga->_node_num;i++)
        sga->final_read_scheme[i] = 0;
    if(sga->_ecN - sga->_ecK == 1) return sga->dist;
    if(BALANCE_SCHEME0 == BASE)
    {//This is the reference group without any optimization
        
        sga->minMaxLoad = 0;
        for(int j = 0;j < sga->_node_num; ++j)
        if(!sga->_failNodeSet[j])
        {
            for(int i = 0;i < sga->failStripeNum; ++i)
            {
                if(sga->dist[sga->stripesToRepair[i]][j])
                {    
                    sga->final_read_scheme[j] += 1;
                }
            }
            
            sga->minMaxLoad = max(sga->final_read_scheme[j],sga->minMaxLoad);
            
        }
        
         fprintf(log_fp, "minMaxLoad:%d\n", sga->minMaxLoad);
         print_vec("base read scheme:", sga->final_read_scheme,sga->_node_num);
        
        return sga->dist;
    
    }
    else if(BALANCE_SCHEME0 == RANDOM)
    {
        sga->rand_dist = init_arr2D(sga->_stripe_num,sga->_node_num);
        
        for(int i = 0;i < sga->_stripe_num; i++)
        for(int j = 0;j < sga->_node_num;j++)
            sga->rand_dist[i][j] = sga->dist[i][j];
        // memcpy(sga->rand_dist,sga->dist,sga->_node_num*sga->_stripe_num*sizeof(int));
        int sum = 0;
        srand(1000007);
        for(int i = 0; i < sga->failStripeNum; ++i)
        {
            int schm_size = vector_size((vector_t *)vector_at(sga->allSchemes,i));
            int pickup = rand()%schm_size;
            vector_t* temp = (vector_t *)vector_at(vector_at(sga->allSchemes,i),pickup);
            for(int k = 0;k < sga->_node_num; k++)
            if(!sga->_failNodeSet[k])
            {
                sga->rand_dist[sga->stripesToRepair[i]][k] = vec_at(temp,k);
                int s = vec_at(vector_at(vector_at(sga->allSchemes,i),pickup),k);
                int y = sga->rand_dist[sga->stripesToRepair[i]][k];
                if(sga->rand_dist[sga->stripesToRepair[i]][k])
                {    
                    sga->final_read_scheme[k] += 1;
                    sum ++;
                }
            }
           
        }
        sga->minMaxLoad = 0;
        for(int j = 0;j < sga->_node_num;j++)
        if(!sga->_failNodeSet[j])
        {
            sga->minMaxLoad = max(sga->minMaxLoad, sga->final_read_scheme[j]);
        }
         fprintf(log_fp, "minMaxLoad:%d, sumLoad :%d\n", sga->minMaxLoad, sum);
         print_vec("final read scheme:", sga->final_read_scheme,sga->_node_num);
        
        return sga->rand_dist;
    }
    else if(BALANCE_SCHEME0 == GREEDY)
    {// random but with greedy heuristic
        //when encountered with a tie, we randomly pick up one
        sga->greedy_dist = init_arr2D(sga->_stripe_num,sga->_node_num);
        for(int i = 0;i < sga->_stripe_num; i++)
        for(int j = 0;j < sga->_node_num;j++)
            sga->greedy_dist [i][j] = sga->dist[i][j];
        // memcpy(sga->greedy_dist,sga->dist,sga->_stripe_num*sga->_node_num*sizeof(int));
        
        
        
        int* stat0 = (int*) calloc( sga->_node_num, sizeof(int));

        vector_t* stats = create_vector(int);//ith stripe each schemes' maxLoad
        vector_init(stats);

        vector_t* ties = create_vector(int);//for ties, randomly pick up one
        vector_init(stats);
        srand(1000009);
        for(int i = 0;i < sga->failStripeNum; ++i)
        {
            sga->minMaxLoad = INT32_MAX;
            vector_clear(stats);
            vector_clear(ties);
            int number_stats = vector_size((vector_t *)vector_at(sga->allSchemes,i));
            for(int i = 0;i < number_stats; i++)
                vector_push_back(stats, 0);
            for(int j = 0;j < number_stats; j++)
            {
                int maxLoad = 0, sumLoad = 0;
                vector_t * tmp = vector_at( vector_at(sga->allSchemes,i) ,j);
                //calc the maxLoad and sumLoad 
                for (int i = 0;i < sga->_node_num;++i )
                if(!sga->_failNodeSet[i])
                {
                    maxLoad = max( maxLoad, stat0[i]);
                    sumLoad += stat0[i];
                }
                vec_at(stats, j) = maxLoad;
                sga->minMaxLoad = min(maxLoad, sga->minMaxLoad);
            }
            int num_tie = 0;
            for(int j = 0;j < number_stats; j++)
            {
                if(vec_at(stats, j) == sga->minMaxLoad)
                {
                    num_tie ++;
                    vector_push_back(ties,j);
                }
            }
            int pickup = rand()%num_tie;
            vector_t * tmp = vector_at( vector_at(sga->allSchemes,i), vec_at(ties,pickup));
            
            for(int k = 0;k < sga->_node_num; k++)
            if(!sga->_failNodeSet[k])
                sga->greedy_dist[sga->stripesToRepair[i]][k] = vec_at(tmp,k);
            //combine two schemes
            for (int i = 0;i < sga->_node_num;++i )
            if(!sga->_failNodeSet[i])
            {
                if(vec_at(tmp,i))
                    stat0[i] += 1;
            }
        }
        for(int i = 0;i < sga->_node_num;i++)
            sga->final_read_scheme[i] = stat0[i];
        
        sga->minMaxLoad = 0;
        int sum = 0;
        for(int j = 0;j < sga->_node_num;j++)
        {
            sga->minMaxLoad = max(sga->minMaxLoad, sga->final_read_scheme[j]);
            sum += sga->final_read_scheme[j];
        }
         fprintf(log_fp, "minMaxLoad:%d, sumLoad :%d\n", sga->minMaxLoad, sum);
         print_vec("final read scheme:", sga->final_read_scheme, sga->_node_num);
        
        free_ex(stat0);
        vector_destroy(stats);
        vector_destroy(ties);
        return sga->greedy_dist;
    }
    else if(BALANCE_SCHEME0 == LOAD_BALANCE)
    {
        //We are going to persist to greedy heuristics
        // avlbleBlcks.resize(failStripeNum,_ecN-_ecK-1);
        // 
        sga->load_blncd_dist = init_arr2D(sga->_stripe_num,sga->_node_num);
        // sga->load_blncd_dist = sga->dist;
        for(int i = 0;i < sga->_stripe_num; i++)
        for(int j = 0;j < sga->_node_num;j++)
            sga->load_blncd_dist [i][j] = sga->dist[i][j];
        //init_arr2D(sga->_stripe_num, sga->_node_num);
        // memcpy(sga->load_blncd_dist,sga->dist,sga->_node_num*sga->_stripe_num*sizeof(int));
        
        
        sga->diskload_dict = init_arr2D(sga->_node_num,sga->failStripeNum);
        int *stripe_redu = (int *) calloc(sga->failStripeNum, sizeof(int));
        int *diskloads = (int *) calloc(sga->_node_num, sizeof(int));
        int *max_reduce_vec = (int*) calloc(sga->_node_num, sizeof(int)); 
        int avlbleSum = (sga->_ecN - sga->_ecK-1)*sga->failStripeNum;
        int last_avlbleSum = 0;
        // vector_resize(sga->diskload_dict, sga->_node_num);
        for(int j = 0;j < sga->_node_num; ++j)
        if(!sga->_failNodeSet[j])
        {
            for(int i = 0;i < sga->failStripeNum; ++i)
            if(sga->dist[sga->stripesToRepair[i]][j])
            {
                sga->diskload_dict[j][i] = 1;
                diskloads[j]++;
            }
            else 
                sga->diskload_dict[j][i] = -1;
        }
        for(int i = 0;i < sga->failStripeNum; i++)
            stripe_redu[i] = sga->_ecN - sga->failNodeNum - sga->_ecK;  
        int torlr_rduc = sga->_node_num-1;// the tolerance for ith maximal load
        int *fail_redu_list = (int *) calloc(sga->_node_num, sizeof(int)); 
        while(avlbleSum>0)
        {
            
            int *temp = (int*) calloc(sga->_node_num,sizeof(int));
            memcpy(temp, diskloads, sga->_node_num*sizeof(int));
            QuickSort(temp, sga->_node_num);
            // print_vec("",temp,sga->_node_num);
            // print_vec("",diskloads,sga->_node_num);
            int cur_max_load = temp[torlr_rduc];
            free_ex(temp); 
            //First reduce the disk with most reducible stripes 
            int  max_reduce = 0;
            memset(max_reduce_vec,-1,sga->_node_num*sizeof(int));
            for(int j = 0;j < sga->_node_num;j++)
            if(!fail_redu_list[j]&& !sga->_failNodeSet[j] && diskloads[j] == cur_max_load)
            {
                int reduce_num = 0;
                for(int i = 0;i < sga->failStripeNum; i++)
                if(sga->diskload_dict[j][i] > 0)
                    reduce_num ++ ;
                if(reduce_num > max_reduce)
                {
                    memset(max_reduce_vec,-1,sga->_node_num*sizeof(int));
                    // print_vec("max_reduce_vec:",max_reduce_vec);
                    max_reduce_vec[j] = 1;
                    max_reduce = reduce_num;
                }
                else if(reduce_num == max_reduce )
                {
                    max_reduce_vec[j] = 1;
                }
            }
            if(all_num(max_reduce_vec,sga->_node_num,-1))
            {
                
                torlr_rduc--;
                continue;
            }
            //if current maximally loaded disk are fully reduced
            //we don't have to judge whether the current maxmimal load is accessible
            bool isMaxReducible = false;
            // print_vec("max_reduce_vec:",max_reduce_vec,sga->_node_num);
            for(int j = 0;j < sga->_node_num;j++)
            if(max_reduce_vec[j] > 0&& !sga->_failNodeSet[j])
            {
                bool flag2  = false;
                for(int k = 0;k  < sga->failStripeNum; k++)
                if(stripe_redu[k] && sga->diskload_dict[j][k]>0)
                {
                    avlbleSum--;
                    stripe_redu[k] --;
                    sga->diskload_dict[j][k] = 0;
                    diskloads[j]--;
                    isMaxReducible = true;
                    flag2 = true;
                    break;
                }
                if(flag2) break;   
                
            
            }
            //if current maximally loaded disk are fully reduced
            //He could borrow some money from previously richest relatives, for illustration
            if(!isMaxReducible)
            {
                for(int j = 0;j < sga->_node_num;j++)
                if(max_reduce_vec[j] > 0&& !sga->_failNodeSet[j])
                {
                    for(int k = 0;k  < sga->failStripeNum; k++)
                    {
                        for(int n = 0;n < sga->_node_num; n++)
                        if(!fail_redu_list[n] && !sga->_failNodeSet[n]&&\
                            sga->dist[sga->stripesToRepair[k]][n] &&\
                            sga->diskload_dict[n][k] == 0 && \
                            sga->diskload_dict[j][k] == 1 && \
                            (diskloads[n] == diskloads[j] - 1))
                            {
                                sga->diskload_dict[n][k] = 1;
                                sga->diskload_dict[j][k] = 0;
                                diskloads[j]--;
                                diskloads[n]++;
                                isMaxReducible = true;
                                break;
                            }
                        if(isMaxReducible) break;
                        
                    }
                    if(avlbleSum == last_avlbleSum)
                    {
                        fail_redu_list[j] = 1;
                    }
                    if(isMaxReducible) break;
                    
                }
            }
            last_avlbleSum = avlbleSum;
            // int* sum = (int *) calloc(sga->_node_num, sizeof(int));
            // for(int j = 0;j < sga->_node_num; ++j)
            // if(!sga->_failNodeSet[j])
            // {
            //     for(int i = 0;i < sga->failStripeNum; ++i)
            //         if(sga->diskload_dict[j][i] > 0) 
            //         {
            //             sum[j]++;
            //         }
            // }
            // printf("avlbleSum:%d\n",avlbleSum);
            // print_vec("sum Blocks:      ", sum,sga->_node_num);


        }
        
        
        sga->minMaxLoad = 0;
        // fprintf(log_fp,"total{");
        for(int i = 0;i < sga->_node_num;i++)
            sga->final_read_scheme[i] = diskloads[i];
        int* sum = (int *) calloc(sga->_node_num, sizeof(int));
        int sumLoad = 0;
        for(int j = 0;j < sga->_node_num; ++j)
        if(!sga->_failNodeSet[j])
        {
            sga->minMaxLoad = max(sga->minMaxLoad, diskloads[j]);
            for(int i = 0;i < sga->failStripeNum; ++i)
            if(sga->diskload_dict[j][i] == 0)
            {
                // printf("stripe:%d,node:%d:%d\n",sga->stripesToRepair[i],j,sga->diskload_dict[j][i]);
                sga->load_blncd_dist[sga->stripesToRepair[i]][j] = 0;
            }
            else if(sga->diskload_dict[j][i] > 0) 
            {
                sum[j]++;
                sumLoad++;
            }
        }
        
        // fprintf(log_fp,"}\n");
        fprintf(log_fp, "minMaxLoad:%d, sumLoad: %d\n", sga->minMaxLoad, sumLoad);
        print_vec("final read scheme:        ", sga->final_read_scheme, sga->_node_num);
        print_vec("sum Blocks:[For verifying]", sum,sga->_node_num);
        // for(int i = 0 ;i < failStripeNum; i++)
        //     print_vec("",load_blncd_dist[stripesToRepair[i]]);
        free_ex(diskloads);
        free_ex(stripe_redu);
        free_ex(max_reduce_vec); 
        free_ex(fail_redu_list);
        return sga->load_blncd_dist;
        
    }
    // else{
    //     //customized Scheme 
    // }
    return sga->dist;
    
}

/**
 * @description: Return a list where the ith element is the disk that ith stripe should be written to. 
 * @param {vector_t*} &dist
 * @return {vector_t} write scheme
 */
void write_load_balancer()
{   
    //if WRITE_TO_NEW_DISK is set false, we have to balance the write loads
    //since write and read are mutually exclusive, the read-intensive disk should write less data
    int wcnt = 0;
    priority_queue_t * pq = create_priority_queue(pair_t<int,int>);
    priority_queue_init(pq);
    
    pair_t * p = create_pair(int,int);
    pair_init(p);
        
    for(int i = 0;i < sga->_node_num;i++)
    if(!sga->_failNodeSet[i])
    {
        pair_make(p, sga->final_read_scheme[i] ,i);
        priority_queue_push(pq, p);
    }
    while(wcnt < sga->failStripeNum)
    {
        pair_t* toppair = priority_queue_top(pq);
        int load = *(int*)pair_first(toppair);
        int node_id = *(int*)pair_second(toppair);
        priority_queue_pop(pq);
        pair_make(p,load+1,node_id);
        priority_queue_push(pq,p);
        sga->final_write_scheme[node_id]++;
        wcnt ++;
    }
    
    priority_queue_destroy(pq);
    pair_destroy(p);
    print_vec("Final_write_scheme:",  sga->final_write_scheme, sga->_node_num);
}

/**
 * @description: ch_ecK whether two stripes could not be repair in parallel 
 * @param {vector_t} a: mask vec of first stripe
 * @param {vector_t} b: mask vec of second stripe
 * @return {bool}
 */
bool isConflict(int* a, int* b, int len)
{
    for(int i = 0;i < len; ++i)
    if(!sga->_failNodeSet[i] && a[i]  && b[i]) 
        return true;
    return false;
}
/**
 * @description: generate a undirected graph, if two stripes conflic, between them form an edge
 * @param {vector_t*} &dist
 * @return 
 */
void graph_generator(int** new_dist)
{
    //first we generate an undirected cycic graph
    //if two nodes mutually exclude(could happen simultaneously), 
    //then between them forms one edge with weight 1.
    // the question transforms into how as many cycles as possible we could find
    //in the graph to cover all the nodes.
    sga->cyclicGraph = init_arr2D(sga->failStripeNum,sga->failStripeNum);
    
    // memset(sga->cyclicGraph,0,sga->failStripeNum*sga->failStripeNum*sizeof(int));
    int num_edges = 0;
    for(int i = 0;i < sga->failStripeNum; i++)
    {
        for(int j = i+1;j < sga->failStripeNum; j++)
        {
            
            if(isConflict(new_dist[sga->stripesToRepair[i]] ,\
                    new_dist[sga->stripesToRepair[j]], \
                     sga->_node_num))
            {//if two nodes conflicts, then add an edge
                sga->cyclicGraph[i][j] = 1;
                sga->cyclicGraph[j][i] = 1;
                num_edges ++;
            }
        }
        
    }
    fprintf(log_fp,"Graph generated, the graph contains %d edges\n",num_edges);   
    if(!num_edges)
    {//Ideally, all stripes could be in parallel repaired
        fprintf(log_fp,"Minimal 1 slices need scheduled\n");
        print_vec("scheme,",sga->stripesToRepair,sga->failStripeNum);
    }
}

/**
 * @description: get the minimal slices for repair
 * @param {vector_t* &} dist: the distribution of stripes
 * @return {int} minimal time slices 
 * @author: Durant Thorvals
 * @complexity: Time:O(V(V+E)),V is the number of stripes,Space:O(V^2)
 */
int getMinimalTime()
{
    sga->stripesColor = (int *) calloc(sga->failStripeNum, sizeof(int));
    

    if(SCHEDULE_SCHEME0 == NONE)
    {//this functions as reference purpose
        fprintf(log_fp,"No Scheduling.\n");
        return sga->failStripeNum;
    }
    if(SCHEDULE_SCHEME0 == COLORING_CLIQUE)
    {
        //we use the disk_vec generated from last step
        //to give assistance to coloring sequence
        
        fprintf(log_fp,"COLORING CLIQUE Scheduling.\n");
        sga->minTimeSlice = 0;
        // memset(sga->stripesColor,0,sga->failStripeNum*sizeof(int));
        
        memset(sga->stripesColor,0,sga->failStripeNum*sizeof(int));
        int* record = (int*)calloc(sga->failStripeNum, sizeof(int));
        for(int i = 0;i < sga->_node_num; i++)
        for(int j = 0;j < sga->failStripeNum; j++)
        if(!sga->stripesColor[j] && sga->diskload_dict[i][j] > 0)
        {
                int cur = j;
                int maxColor = 0;
                memset(record,0,sga->failStripeNum*sizeof(int));
                for(int neig = 0;neig < sga->failStripeNum; neig++)
                if(sga->cyclicGraph[cur][neig])
                {
                    record[sga->stripesColor[neig]] = 1;
                    maxColor = max(maxColor, sga->stripesColor[neig]);
                }
                for(int t = 1;t <= maxColor+1; t++)
                {
                    if(!record[t]) {
                        sga->stripesColor[cur] = t;
                        sga->minTimeSlice = max(sga->minTimeSlice, t);
                        break;
                    }    
                }
                
        }
        free_ex(record);
        sga->matchStripeSet = init_arr2D(sga->minTimeSlice, sga->_stripe_num);
        // print_vec("", sga->stripesToRepair, sga->failStripeNum);
        for(int t = 0; t < sga->minTimeSlice; t++)
        {
            // fprintf(log_fp, "{");
            int cnt = 0;
            for(int i = 0;i < sga->failStripeNum; i++)
            {
                if(sga->stripesColor[i] == t+1)
                {
                    sga->matchStripeSet[t][sga->stripesToRepair[i]] = 1;
                    cnt++;
                    // fprintf(log_fp,"%d, ", i);
                    // fprintf(log_fp, "stripe%2d:", i);
                    // for(int j = 0;j < sga->_node_num; j++)
                    // {
                    //     fprintf(log_fp, "%*d ",2, sga->dist[sga->stripesToRepair[i]][j]);
                    // }
                    // fprintf(log_fp,"\n");
                }
                
            }
            // fprintf(log_fp, "}\n");
            // fprintf(log_fp, "the %d slice contains %d stripes\n",t,cnt);
        }
        fprintf(log_fp,"Minimal %d slices need scheduled\n", sga->minTimeSlice);
        
        return sga->minTimeSlice;
    }
    else if(SCHEDULE_SCHEME0 == COLORING_SEQ)
    {
        fprintf(log_fp,"COLORING SEQUENCE Scheduling.\n");
        sga->minTimeSlice = 0;
        //Greedy-heuristically coloring the graph
        //A commonly used ordering for greedy coloring is to choose a vertex v of minimal degree
        //,order the subgraph with v removed recursively, and then place v last in the graph.
        //The largest degree of a removed vertex is called degenerancy of the graph
        //When there are many forests, Que devrais tu faire?
        memset(sga->stripesColor,0,sga->failStripeNum*sizeof(int));
        int* record = (int*)calloc(sga->failStripeNum, sizeof(int));
        for(int i = 0;i < sga->failStripeNum;++i)
        if(!sga->stripesColor[i])
        {
                int cur = i;
                int maxColor = 0;
                memset(record,0,sga->failStripeNum*sizeof(int));
                for(int neig = 0;neig < sga->failStripeNum; neig++)
                if(sga->cyclicGraph[cur][neig])
                {
                    record[sga->stripesColor[neig]] = 1;
                    maxColor = max(maxColor, sga->stripesColor[neig]);
                }
                for(int t = 1;t <= maxColor+1; t++)
                {
                    if(!record[t]) {
                        sga->stripesColor[cur] = t;
                        sga->minTimeSlice = max(sga->minTimeSlice, t);
                        break;
                    }    
                }
                
        }
        free_ex(record);
        sga->matchStripeSet = init_arr2D(sga->minTimeSlice, sga->_stripe_num);
        // print_vec("", sga->stripesToRepair, sga->failStripeNum);
        for(int t = 0; t < sga->minTimeSlice; t++)
        {
            // fprintf(log_fp, "{");
            int cnt = 0;
            for(int i = 0;i < sga->failStripeNum; i++)
            {
                if(sga->stripesColor[i] == t+1)
                {
                    sga->matchStripeSet[t][sga->stripesToRepair[i]] = 1;
                    cnt++;
                    // fprintf(log_fp,"%d, ", i);
                    // fprintf(log_fp, "stripe%2d:", i);
                    // for(int j = 0;j < sga->_node_num; j++)
                    // {
                    //     fprintf(log_fp, "%*d ",2, sga->dist[sga->stripesToRepair[i]][j]);
                    // }
                    // fprintf(log_fp,"\n");
                }
                
            }
            // fprintf(log_fp, "}\n");
            // fprintf(log_fp, "the %d slice contains %d stripes\n",t,cnt);
        }
        fprintf(log_fp,"Minimal %d slices need scheduled\n", sga->minTimeSlice);
        
        return sga->minTimeSlice;
    }
    else if(SCHEDULE_SCHEME0 == COLORING_BFS)
    {
        fprintf(log_fp,"COLORING BFS Scheduling.\n");
        // minTimeSlice = 0;
        // //Greedy-heuristically coloring the graph
        // //A commonly used ordering for greedy coloring is to choose a vertex v of minimal degree
        // //,order the subgraph with v removed recursively, and then place v last in the graph.
        // //The largest degree of a removed vertex is called degenerancy of the graph
        // //When there are many forests, Que devrais tu faire?
        // std::queue<int> q;
        // stripesColor.clear();
        // stripesColor.resize(failStripeNum,0);
        
        // for(int i = 0;i < failStripeNum;++i)
        // if(!stripesColor[i])
        // {
        //     q.push(i);//the index of stripesToRepair
        //     while(!q.empty())
        //     {
        //         auto cur= q.front();q.pop();
        //         if(stripesColor[cur]) continue;
        //         int maxColor = 0;
        //         std::unordered_set<int> record;
        //         for(auto &neig: cyclicGraph[cur])
        //         {
        //             record.insert(stripesColor[neig]);
        //             maxColor = std::max(maxColor, stripesColor[neig]);
        //             if(!stripesColor[neig]) q.push(neig);
        //         }
        //         for(int t = 1;t <= maxColor+1; t++)
        //         {
        //             if(!record.count(t)) {
        //                 stripesColor[cur] = t;
        //                 minTimeSlice = std::max(minTimeSlice, t);
        //                 break;
        //             }    
        //         }
        //         // printf("cur:%d, color:%d,used:%d\n",cur, stripesColor[cur],(int)used.size());
                
        //     }
        // }
        // matchStripeSet.clear();
        // //each subarray in final scheme should be less than maxRepairNumPerSlice
        // for(int t = 1; t <= minTimeSlice; t++)
        // {
        //     vec1u32 temp;
        //     // fprintf(log_fp, "{");
        //     for(int i = 0;i < failStripeNum; i++)
        //     {
        //         if(stripesColor[i] == t)
        //         {
        //             temp.emplace_back(stripesToRepair[i]);
        //             // fprintf(log_fp,"%d, ", i);
        //             // fprintf(log_fp, "stripe%2d:", i);
        //             // for(int j = 0;j < _node_num; j++)
        //             // {
        //             //     fprintf(log_fp, "%*d ",2, dist[stripesToRepair[i]][j]);
        //             // }
        //             // fprintf(log_fp,"\n");
        //         }
                
        //     }
            

        //     // fprintf(log_fp, "}\n");
        //     // fprintf(log_fp, "the %d slice contains %d stripes\n",t,(int)temp.size());
        //     matchStripeSet.push_back(temp);
        // }
        // fprintf(log_fp,"Minimal %d slices need scheduled\n", minTimeSlice);
        
        // return minTimeSlice;
    }
    else if(SCHEDULE_SCHEME0 == MAX_CLIQUE)
    {   //This is somewhat at-your-fingertips methods in real production
        //let's test its efficiency

        //The idea is trying to put as many compatible stripes in one clique as possible.
        //And find the total time in accordance with maximal clique

    }
    
    return -1;
}


