/*
 * @Author: Durant Thorvalds
 * @Date: 2021-06-25 00:03:57
 * @LastEditTime: 2021-07-19 20:24:20
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /LoadBalancerSim/include/ECconfig.h
 */
#ifndef __ECCONFIG_H_
#define __ECCONFIG_H_

#ifdef __cplusplus
extern "C"{
#endif
//PLEASE asure this is consistent with parv file settings
//DOESNOT CONTAIN BACKUP DEV
// #define node_num 8 
// #define ecK  2
// #define ecM  2
// #define ecN  (ecK+ecM)   
// #define stripe_num  10000
 
//the failed disk number labelled from 1
// #define failed_diskno   1 //,\
                        // 2,\
                        // 3 
//Tree-test only, otherwise unused
// #define PARTITION 25 
// #pragma GCC optimize(2)
// #define DEBUG_MODE false
// #define LOG_MODE true
//whether generate (true) or read distribution(false)
// #define GEN_OR_READ_DIST false
//if you have distribution on the disk, please designate the following path
// #define DIST_PATH "/home/disksim/DISKSIM64_C/dist/dist.data" 
//where yu want to print the log, to file fp or console("stdout")
// #define LOG_PATH  "/home/disksim/DISKSIM64_C/logs/log.txt" //"stdout" 
// #define RAND_SEED 2021
//whether to write reconstructed data to new disks or existent disk
#define WRITE_TO_NEW_DISK true
//Where to generate the tracefile
// #define IOTRACE_PATH "/home/disksim/DISKSIM64_C/traces/ascii_fail_only.trace"
#define MAX_SCHEME_NUM 500
#ifdef __cplusplus
}
#endif

#endif
