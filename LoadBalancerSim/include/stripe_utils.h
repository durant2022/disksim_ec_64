#ifndef __STRIPE_UTILS_H
#define __STRIPE_UTILS_H

#ifdef __cplusplus
extern "C"{
#endif

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <errno.h>
#include <stdint.h>
#include <stddef.h>
#include "ECconfig.h"
#include <cstl/cstl_def.h>
// #include <cstl/citerator.h>
// #include <cstl/cstl_vector_iterator.h>
#include <cstl/cvector.h>
// #include <cstl/clist.h>
// #include <cstl/chash_set.h>
// #include <cstl/chash_map.h>
#include <cstl/calgorithm.h>    
#include <cstl/cqueue.h>
#include <cstl/cstl_pair.h>
#include <disksim_global.h>
// #include <cstl/cstl_multimap.h>
// #include <cstl/cstl_function.h>
// #define max(x,y) (x)>(y)?(x):(y)
// #define min(x,y) (x)<(y)?(x):(y)

#define talloc(type,num) (type*) malloc(sizeof(type)*(num))
#define get_ele(arr,row,col) *(*(arr+row)+col)
#define vec_at(V,I) *(int*)vector_at(V,I)
#define vec2_at(V,I,J) *(int*)vector_at(vector_at(V,I),J)
// #define print_vec(S,x) {fprintf(stdout,"%s{",S);\ 
//                     for(int i = 0;i < n;i++)\
//                         fprintf(stdout, "%d\n",*(int *)vector_at(S,i)); \
//                     fprintf(stdout,"},\n");}

typedef enum load_balance_method
{
    BASE,
    LOAD_BALANCE,
    RANDOM,
    GREEDY,
    TREE_GREEDY,
    USER_DEFINE,
} BALANCE_SCHEME;

typedef enum IO_scheduling_method
{
    NONE,
    MAX_CLIQUE,
    COLORING_SEQ,
    COLORING_CLIQUE,
    COLORING_BFS,
    
}SCHEDULE_SCHEME;



typedef struct Stripe
{
    
    int _node_num;//num of node
    int _ecN;//erasure code N = k+m
    int _ecK;//erasure code K
    int _stripe_num;// num of stripes
    int _rand_seed;
    int failNodeNum;//num of nodes in failed node(s)
    int _maxRepairNumPerSlice;
    int minTimeSlice;
    int _maxStripeRepairPerSlice;

    int ** dist;//parity_table

    int* _failNodeSet;//for disks
    int* _backupSet;//for backups, one backup by one disk
    int *sumBlocks;
    int** cyclicGraph;
    // int* degree;
    int* stripesToRepair;
    int* stripesColor;
    int** matchStripeSet;
    int failStripeNum;
    int minMaxLoad;
    int** load_blncd_dist;
    int* final_read_scheme;
    int* final_write_scheme;
    
    int** rand_dist;
    int** greedy_dist;
    vector_t* allSchemes;
    int **diskload_dict;
    // public :

    
} SGA;

extern BALANCE_SCHEME BALANCE_SCHEME0;
extern SCHEDULE_SCHEME SCHEDULE_SCHEME0;
extern SGA *sga;
extern FILE* log_fp;
extern int **new_dist;
extern double repair_start;
extern double repair_end;
extern int failed_diskno;
extern char* DIST_PATH;
//qsort.c
void QuickSort(int *vec, int len);
//stripeSGC.c
void transfer(char * src1, char *src2);
void free_arr2D(int** arr, int len);
int** init_arr2D(int row, int col);
void set_params( int _node_num, int _ecN, 
                                int _ecK, int _stripe_num, 
                                int _rand_seed, 
                                int _maxStripeRepairPerSlice);

int ** generate_random_distributions( bool print);
void print_dist(int** dist );
void print_vec(char* prefix, int *vec, int len);
void write_distribution_to_disk( char* path);
void  read_distribution_from_disk(char *path);
bool checkStripeRepair();
void all_schemes_generator();
void generate_default_repair_schemes( int start, int cntK, int *rowVec, int stripe);

int** IO_load_balancer();
void write_load_balancer();
bool isConflict(int* a, int* b, int len);
void graph_generator(int** new_dist);
int getMinimalTime();
// void write_distribution_to_disk(int* &dist, std::string path);
// int* read_distribution_from_disk(std::string path);

// bool checkStripeRepair(int* &dist,const int* &_failedList);
// bool isConflict(vec1u16 a, vec1u16 b);

// //Core Function 
// //IO-Scheduler
// void graph_generator(int* &dist);
// int getMinimalTime(int* &dist);
// //Read-IO-Balancer
// int* IO_load_balancer(int* &dist, int* repairSchemes);
// //Wrtie-IO-Balancer
// vec1u16 write_load_balancer();
// //unrelavant to the main idea, for test propose only↓
// void generate_default_repair_schemes(int start,int cntK,vec1u16 &rowVec, int* &schemes);
// void all_schemes_generator(int* &dist);
// int load_metric(vec1u16 stat);
// int* combineScheme(int* &cur_schm, vec1u16 &add, int &maxload, int &sumLoad);
// int* combineScheme(int* &cur_schm, vec1u16 &add);
// vec1u16 recoverScheme(int* &cur_schm, int* &last_schm);


#ifdef __cplusplus
}
#endif

#endif
