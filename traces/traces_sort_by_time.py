'''
Author: your name
Date: 2021-07-07 00:44:54
LastEditTime: 2021-07-07 01:27:24
LastEditors: Please set LastEditors
Description: In User Settings Edit
FilePath: /DISKSIM64_C/traces/traces_sort_by_time.py
'''
#!/usr/bin/python3

import sys
import getopt

def _usage():
    print("sort the trace IO according to time ascending order")
    print("-p   trace_file_path     equ --path")
 
def main(argv):
	
    try:
        opts,args=getopt.getopt(argv, "hp:", ["help","path="])
    except getopt.GetoptError:
        _usage()
        sys.exit()

    path = "./ascii.trace"
    for opt, arg in opts:
        if opt in ("-h", "--help"):
            _usage()
            sys.exit()
        # -n与--name等价
        elif opt in ("-p", "--path"):
            path = arg
    sort_ = []
    with open(path,"r") as f:
        for line in f.readlines():
            sort_.append(line)
            # print("data:%s"%(line))
            
    sort_ = sorted(sort_, key=lambda x: float( x.split("\t")[0]))
    with open(path,"w") as f:
        
        f.writelines(sort_)
            
if __name__ == "__main__":
    main(sys.argv[1:])