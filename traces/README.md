If you want to simulate normal IO trace, just use synthio module, but saddly it's not ordered in time sequence so I prepare you the tiny script `trace_sort_by_time.py` to sort the traces by time.
	usage: python3 ./traces_sort_by_time.py {tracefile name}
tracefile name default to be ascii.trace if not set. I hope it works.


If you want to generate specific events like disk failure. Insert manually a trace entry in format:
	<time_stamp>	<failure_flag>	<dev_no>
for exmaple 
	0.01	F	1 
represents disk 1 fails.

Actually, there should be at LEAST two traces entries, since prime_simulation() would check the trace first, suggest at least 2 traces for disk failure experiment.For example, a normal one followed with failed one.

Cautions:
Mind the tabs!
More fun under development.
