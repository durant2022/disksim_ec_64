#include <iostream>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

int main(){
    string line;
    vector<vector<string> > str_vec;
    for (int i = 0; i < 3; i++) {
        vector<string> vec;
        str_vec.push_back(vec);
    }
    string::iterator it;
    ifstream file("op.txt");
    int cnt = 0;
    int cnt_i = 0, cnt_j = 0;
    
    while (getline(file, line)) {
        cnt++;
        for (it = line.begin(); it != line.end(); it++) {
            if (*it > '9' || *it < '0') {
                line.erase(it);
                it--;
            } else {
                string::size_type position = line.find(",");
                if (position != line.npos) {
                    line = line.substr(0, position);
                }
                //cout << line << endl;
                str_vec[cnt_i].push_back(line);
                cnt_j++;
                if (cnt_j == 6) {
                    cnt_j = 0;
                    cnt_i++;
                }
                break;
            }
        }
        if (cnt == 6) {
            //cout << endl;
            cnt = 0;
        }
    }
    //cout << "====== test ======" << endl;
    cout << endl;
    for (int i = 0; i < 6; i++) {
        for(int j = 0; j < 3; j++) {
            cout << str_vec[j][i] << " ";
            if (j == 2) cout << endl;
        }
    }

    return 0;
}
