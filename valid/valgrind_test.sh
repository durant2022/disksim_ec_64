#!/bin/bash
###
 # @Author: Durant Thorvalds
 # @Date: 2021-07-13 20:30:55
 # @LastEditTime: 2021-07-15 23:44:41
 # @LastEditors: Please set LastEditors
 # @Description: In User Settings Edit
 # @FilePath: /DISKSIM64_C/valid/sga_runvalid.sh
### 

PREFIX=..
ecK=6
ecM=3
stripenum=5000
DISKNUM=32
OUT_PREFIX=disknum_${DISKNUM}
OUTV=${OUT_PREFIX}/synthSGA_${a}_${b}.outv

echo -----------------------------------------------
echo "VALGRIND test for disksim, fucking memory leaks"

# valgrind --tool=memcheck --leak-check=yes --show-reachable=yes --run-libc-freeres=yes --log-file=./valgrind_report.log \
                ${PREFIX}/src/disksim \
                    synthSGA_${DISKNUM}.parv \
                    ${OUT_PREFIX}/synthSGA_lb_none.outv \
                    ascii \
                    ${PREFIX}/traces/ascii_fail_only.trace \
                    0 \
                    ${PREFIX}/logs/synthSGA_log_lb_none.log \
                    load_balance \
                    none \
                    ${ecK}  \
                    ${ecM}  \
                    ${stripenum} \
                    read_dist \
                    ${PREFIX}/dist/dist_${ecK}_${ecM}_${stripenum}.data \
                    2021 \
                   