#!/bin/bash
###
 # @Author: Durant Thorvalds
 # @Date: 2021-07-13 20:30:55
 # @LastEditTime: 2021-07-20 01:12:10
 # @LastEditors: Please set LastEditors
 # @Description: In User Settings Edit
 # @FilePath: /DISKSIM64_C/valid/sga_runvalid.sh
### 

PREFIX=..
ecK=16
ecM=4
stripenum=5000
DISKNUM=200
OUT_PREFIX=disknum_${DISKNUM}
OUT_PUT_FILE=op.txt

if [[ $# -eq 1 ]] && [[ $1 == "-C" ]];then
    echo "setup compiling "
    cd ${PREFIX}
    make clean&&make
    cd ./valid
fi
echo -----------------------------------------------
echo "These results represent actual drive validation experiments of SGA"
echo "If you need to recompile the source, append -C "
echo " "
# lb_scheme=(load_balance greedy random)
# coloring=(coloring_seq none)

rm ${OUT_PUT_FILE}

echo "balance_scheme:load-balance, scheduling_scheme:coloring "
${PREFIX}/src/disksim synthSGA_${DISKNUM}.parv ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv ascii \
            ${PREFIX}/traces/ascii_fail_only.trace \
            0 \
            ${PREFIX}/logs/synthSGA_log_load_balance_coloring.log \
            load_balance \
            coloring_seq \
            ${ecK}  \
            ${ecM}  \
            ${stripenum} \
            gen_dist \
            ${PREFIX}/dist/dist_${ecK}_${ecM}_${stripenum}.data \
            2021 \
            &&grep "Total time of run" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Requests per second" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Response time average" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Requests per second" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Response time average" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} \
            &&grep "minMaxLoad" ${PREFIX}/logs/synthSGA_log_load_balance_coloring.log >> ${OUT_PUT_FILE} \
            &&grep "SGA REPAIR:totally" ${OUT_PREFIX}/synthSGA_load_balance_coloring.outv >> ${OUT_PUT_FILE} 
echo "done"
echo " "
echo " " >> ${OUT_PUT_FILE} 


echo "balance_scheme:greedy, scheduling_scheme: none"
    ${PREFIX}/src/disksim synthSGA_${DISKNUM}.parv ${OUT_PREFIX}/synthSGA_greedy_none.outv ascii \
            ${PREFIX}/traces/ascii_fail_only.trace \
            0 \
            ${PREFIX}/logs/synthSGA_log_greedy_none.log \
            greedy \
            none \
            ${ecK}  \
            ${ecM}  \
            ${stripenum} \
            read_dist \
            ${PREFIX}/dist/dist_${ecK}_${ecM}_${stripenum}.data \
            2021 \
            &&grep "Total time of run" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Requests per second" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Response time average" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Requests per second" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Response time average" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} \
            &&grep "minMaxLoad" ${PREFIX}/logs/synthSGA_log_greedy_none.log >> ${OUT_PUT_FILE} \
            &&grep "SGA REPAIR:totally" ${OUT_PREFIX}/synthSGA_greedy_none.outv >> ${OUT_PUT_FILE} 
echo "done"
echo " "
echo " " >> ${OUT_PUT_FILE}
    
echo "balance_scheme:random, scheduling_scheme: none"
    ${PREFIX}/src/disksim synthSGA_${DISKNUM}.parv ${OUT_PREFIX}/synthSGA_random_none.outv ascii \
            ${PREFIX}/traces/ascii_fail_only.trace \
            0 \
            ${PREFIX}/logs/synthSGA_log_random_none.log \
            random \
            none \
            ${ecK}  \
            ${ecM}  \
            ${stripenum} \
            read_dist \
            ${PREFIX}/dist/dist_${ecK}_${ecM}_${stripenum}.data \
            2021 \
            &&grep "Total time of run" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Requests per second" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Overall I/O System Response time average" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Requests per second" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} \
            &&grep "Disk Response time average" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} \
            &&grep "minMaxLoad" ${PREFIX}/logs/synthSGA_log_random_none.log >> ${OUT_PUT_FILE} \
            &&grep "SGA REPAIR:totally" ${OUT_PREFIX}/synthSGA_random_none.outv >> ${OUT_PUT_FILE} 
echo "done"
echo " "
echo " " >> ${OUT_PUT_FILE}    