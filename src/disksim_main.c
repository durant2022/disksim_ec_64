/*
 * @Author: your name
 * @Date: 2021-06-22 23:52:13
 * @LastEditTime: 2021-07-19 20:23:59
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /DISKSIM64_C/src/disksim_main.c
 */
/*
 * DiskSim Storage Subsystem Simulation Environment (Version 4.0)
 * Revision Authors: John Bucy, Greg Ganger
 * Contributors: John Griffin, Jiri Schindler, Steve Schlosser
 *
 * Copyright (c) of Carnegie Mellon University, 2001-2008.
 *
 * This software is being provided by the copyright holders under the
 * following license. By obtaining, using and/or copying this software,
 * you agree that you have read, understood, and will comply with the
 * following terms and conditions:
 *
 * Permission to reproduce, use, and prepare derivative works of this
 * software is granted provided the copyright and "No Warranty" statements
 * are included with all reproductions and derivative works and associated
 * documentation. This software may also be redistributed without charge
 * provided that the copyright and "No Warranty" statements are included
 * in all redistributions.
 *
 * NO WARRANTY. THIS SOFTWARE IS FURNISHED ON AN "AS IS" BASIS.
 * CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND, EITHER
 * EXPRESSED OR IMPLIED AS TO THE MATTER INCLUDING, BUT NOT LIMITED
 * TO: WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY, EXCLUSIVITY
 * OF RESULTS OR RESULTS OBTAINED FROM USE OF THIS SOFTWARE. CARNEGIE
 * MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF ANY KIND WITH RESPECT
 * TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT INFRINGEMENT.
 * COPYRIGHT HOLDERS WILL BEAR NO LIABILITY FOR ANY USE OF THIS SOFTWARE
 * OR DOCUMENTATION.
 *
 */



/*
 * DiskSim Storage Subsystem Simulation Environment (Version 2.0)
 * Revision Authors: Greg Ganger
 * Contributors: Ross Cohen, John Griffin, Steve Schlosser
 *
 * Copyright (c) of Carnegie Mellon University, 1999.
 *
 * Permission to reproduce, use, and prepare derivative works of
 * this software for internal use is granted provided the copyright
 * and "No Warranty" statements are included with all reproductions
 * and derivative works. This software may also be redistributed
 * without charge provided that the copyright and "No Warranty"
 * statements are included in all redistributions.
 *
 * NO WARRANTY. THIS SOFTWARE IS FURNISHED ON AN "AS IS" BASIS.
 * CARNEGIE MELLON UNIVERSITY MAKES NO WARRANTIES OF ANY KIND, EITHER
 * EXPRESSED OR IMPLIED AS TO THE MATTER INCLUDING, BUT NOT LIMITED
 * TO: WARRANTY OF FITNESS FOR PURPOSE OR MERCHANTABILITY, EXCLUSIVITY
 * OF RESULTS OR RESULTS OBTAINED FROM USE OF THIS SOFTWARE. CARNEGIE
 * MELLON UNIVERSITY DOES NOT MAKE ANY WARRANTY OF ANY KIND WITH RESPECT
 * TO FREEDOM FROM PATENT, TRADEMARK, OR COPYRIGHT INFRINGEMENT.
 */


#include "disksim_global.h"
#include  "sys/time.h"
#include  <stdio.h>
#include  <time.h>
#include  <stdint.h>

SGA *sga;
FILE* log_fp;
FILE* iogen_fp;
BALANCE_SCHEME BALANCE_SCHEME0;
SCHEDULE_SCHEME SCHEDULE_SCHEME0;

double rcw_calc_parity_time = 0.0;//for single stripe
double rmw_calc_parity_time = 0.0;//for single stripe
double sga_issure_intrvl = 0.0;
double sga_prepare_time = 0.0;

bool DEGRADE_FLAG = false;
bool TABLE_REMAP_FLAG = true;
double repair_start = 0.0;
double repair_end = 0.0;
double TIME_PENALTY = 0.0;

int REPAIR_TIMES = 0;
int failed_diskno = -1;
int mapiocnt = 0;
int ** new_dist;
int ** getstripeno;
int repaired_stripe_cnt = 0;
int repair_block_cnt = 0;
int repair_time_cnt = 0;
int avlblebackup = -1;
int ecK;
int ecM;
int ecN;
int stripe_num;

void print_curr_time()
{
  struct timeval tv;
  gettimeofday(&tv,NULL);//get time interval from 1970-1-1 to now
  uint64_t sec=tv.tv_sec;
  uint64_t min=tv.tv_sec/60;
  struct tm cur_tm;//save transformed data
  localtime_r((time_t*)&sec,&cur_tm);
  char cur_time[20];
  snprintf(cur_time,20,"%d-%02d-%02d %02d:%02d:%02d",cur_tm.tm_year+1900,cur_tm.tm_mon+1,cur_tm.tm_mday,cur_tm.tm_hour,cur_tm.tm_min,cur_tm.tm_sec);
  fprintf(log_fp, "%s\n",cur_time);//print current time

}


int main (int argc, char **argv)
{


  setlinebuf(stdout);
  setlinebuf(stderr);
  
  if(argc == 2) {
     disksim_restore_from_checkpoint (argv[1]);
  } 
  else {
    disksim = calloc(1, sizeof(struct disksim));
    disksim_initialize_disksim_structure(disksim);
    disksim_setup_disksim (argc, argv);
  }
  disksim_run_simulation ();
  disksim_cleanup_and_printstats ();

  free_arr2D(getstripeno,sga->_stripe_num);
  destroy_sga();
  free(sga);
  sga = NULL;
  fflush(log_fp);
  fclose(log_fp);
  if(disksim->synthgen) {
    fflush(iogen_fp);
    fclose(iogen_fp);
  }
      
  exit(0);
}
